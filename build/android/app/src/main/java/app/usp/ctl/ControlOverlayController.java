/*
Portable ZX-Spectrum emulator.
Copyright (C) 2001-2020 SMT, Dexus, Alone Coder, deathsoft, djdron, scor, MegaMraz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package app.usp.ctl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.os.SystemClock;
import app.usp.Emulator;

public class ControlOverlayController extends ControlOverlay
{
	private final int size;
	private final int ptr_size;
	private final int size_pot;

	private long touch_joy_time = 0;
	private float dir_x = 0.0f;
	private float dir_y = 0.0f;
	private int pid_joy = -1;
	private int sprite = -1;

	private final int[] textures = new int[4];
	private Bitmap joy_area;
	private Bitmap joy_area_isometric;
	private Bitmap joy_ptr;
	private Bitmap weapon_rack;

	private final int texJoy = 0;
	private final int texJoyIsometric = 1;
	private final int texButton = 2;
	private final int texWeaponRack = 3;

	private int width = 0;
	private int height = 0;

	float padSize;
	float halfHeight;
	float halfWidth;
	float halfPadSize;
	float halfSize;
	float lowerQuarter;
	float upperQuarter;
	int padShift;

	int isometric;
	
	public ControlOverlayController(Context context)
	{
		size = (int)(context.getResources().getDisplayMetrics().density*150);

		padSize = size * 1.3f;
		halfPadSize = padSize * 0.5f;
		halfSize = size * 0.5f;

		ptr_size = (int)(size*0.4f);
		size_pot = NextPot(size);

		DrawTextures();
	}
	private void DrawTextures()
	{
		Paint paint = new Paint();
		paint.setAntiAlias(true);

		int accentColor = Color.GREEN;

		joy_area = Bitmap.createBitmap(size_pot, size_pot, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(joy_area);
		paint.setColor(Color.GRAY);
		canvas.drawCircle(size*0.5f, size*0.5f, size*0.45f, paint);
		paint.setColor(accentColor);
		paint.setStrokeWidth(8);
		canvas.drawLine(halfSize, 0, halfSize, size_pot, paint);
		canvas.drawLine(0, halfSize, size_pot, halfSize, paint);

		joy_area_isometric = Bitmap.createBitmap(size_pot, size_pot, Bitmap.Config.ARGB_8888);
		canvas = new Canvas(joy_area_isometric);
		paint.setColor(Color.GRAY);
		canvas.drawCircle(size*0.5f, size*0.5f, size*0.45f, paint);
		paint.setColor(accentColor);
		paint.setStrokeWidth(8);
		int dipstickC = (int)(size * 0.18f);
		canvas.drawLine(dipstickC, dipstickC, size - dipstickC, size - dipstickC, paint);
		canvas.drawLine(dipstickC, size - dipstickC, size - dipstickC, dipstickC, paint);


		joy_ptr = Bitmap.createBitmap(size_pot, size_pot, Bitmap.Config.ARGB_8888);
		canvas = new Canvas(joy_ptr);
		paint.setColor(accentColor);
		canvas.drawCircle(size*0.5f, size*0.5f, ptr_size*0.5f, paint);

		weapon_rack = Bitmap.createBitmap(size_pot, size_pot, Bitmap.Config.ARGB_8888);
		canvas = new Canvas(weapon_rack);
		float digitSize = size / 3.0f;
		float halfDigitSize = digitSize / 2.0f;
		float dipstick = digitSize * 0.8f;
		for (int j = 0; j < 3; j++)
			for (int i = 0; i < 2; i++)
			{
				paint.setColor(accentColor);
				canvas.drawCircle(digitSize * i + halfDigitSize, digitSize * j + halfDigitSize, halfDigitSize, paint);
				paint.setColor(Color.BLACK);
				paint.setTextSize(digitSize);
				String text = i == 1 && j == 2 ? "E" : Integer.toString(j * 2 + i + 1);
				canvas.drawText(text, digitSize * i + dipstick / 4.0f, digitSize * j + dipstick, paint);
			}
	}
	public void Init()
	{
		GLES20.glGenTextures(4, textures, 0);

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[texJoy]);
	    GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, joy_area, 0);

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[texJoyIsometric]);
		GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, joy_area_isometric, 0);

	    GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[texButton]);
	    GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, joy_ptr, 0);

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[texWeaponRack]);
		GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, weapon_rack, 0);

		sprite = Emulator.the.GLSpriteCreate(size, size);
	}
	public void ChangeIsometric()
	{
		isometric = Emulator.the.GetOptionInt("isometric");
	}
	public void SetDimensions(int height, int width)
	{
		this.width = width;
		this.height = height;

		halfHeight = height * 0.5f;
		halfWidth = width * 0.5f;

		upperQuarter = height / 6.0f * 5.0f;
		lowerQuarter = height / 6.0f;

		padShift = 0; //(int)(halfHeight - halfSize);
	}
	public void OnTouch(float x, float y, boolean down, int pointer_id)
	{
		if(!active)
			return;
		touch_time = SystemClock.uptimeMillis();

		if (down &&
				x < halfWidth &&
				y > padShift &&
				y < padShift + padSize)
		{
			touch_joy_time = touch_time;
			pid_joy = pointer_id;
			float dx = x - halfSize;
			float dy = y - padShift - halfSize;
			final float dir_len = (float)Math.sqrt(dx*dx + dy*dy);
			float scale = dir_len / (halfSize);
			if(scale > 1.0f)
				scale = 1.0f;
			dx *= scale/dir_len;
			dy *= scale/dir_len;

			boolean touchLeft = dx < -0.3f;
			boolean touchRight = dx > +0.3f;
			boolean touchUp = dy > +0.3f;
			boolean touchDown = dy < -0.3f;

			switch (isometric) {
				case 1: //left
					if (touchUp & !touchDown & !touchLeft & !touchRight) {
						Control.UpdateJoystickKeys(false, true, true, false);
					} else if (touchDown & !touchUp & !touchLeft & !touchRight) {
						Control.UpdateJoystickKeys(true, false, false, true);
					} else if (touchLeft & !touchRight & !touchUp & !touchDown) {
						Control.UpdateJoystickKeys(true, false, true, false);
					} else if (touchRight & !touchLeft & !touchUp & !touchDown) {
						Control.UpdateJoystickKeys(false, true, false, true);
					} else {
						Control.UpdateJoystickKeys(touchLeft & touchDown,
								touchRight & touchUp,
								touchLeft & touchUp,
								touchRight & touchDown);
					}
					break;
				case 2: //right
					if (touchUp & !touchDown & !touchLeft & !touchRight) {
						Control.UpdateJoystickKeys(true, false, true, false);
					} else if (touchDown & !touchUp & !touchLeft & !touchRight) {
						Control.UpdateJoystickKeys(false, true, false, true);
					} else if (touchLeft & !touchRight & !touchUp & !touchDown) {
						Control.UpdateJoystickKeys(true, false, false, true);
					} else if (touchRight & !touchLeft & !touchUp & !touchDown) {
						Control.UpdateJoystickKeys(false, true, true, false);
					} else {
						Control.UpdateJoystickKeys(touchLeft & touchUp,
								touchRight & touchDown,
								touchRight & touchUp,
								touchLeft & touchDown);
					}
					break;
				default:
					Control.UpdateJoystickKeys(touchLeft, touchRight, touchUp, touchDown);
					break;
			}

			dir_x = dx;
			dir_y = dy;
		}
		else if (pointer_id == pid_joy)
		{
			pid_joy = -1;
			Emulator.the.OnKey('r', false, false, false);
			Emulator.the.OnKey('l', false, false, false);
			Emulator.the.OnKey('u', false, false, false);
			Emulator.the.OnKey('d', false, false, false);
		}
		else if (x > halfWidth)
		{
			if (y < lowerQuarter + halfSize)
			{
				Emulator.the.OnKey(' ', down, false, false);
			}
			else if (y > upperQuarter - halfSize && isometric == 0)
			{
				Emulator.the.OnKey('u', down, false, false);
			}
			else {
				Emulator.the.OnKey('f', down, false, false);
			}
		}
		else if (x < padSize &&
				 y > height - padSize &&
		         y < height - padSize + size) {
			int gridX = (int)(Math.floor(x / (size / 3.0f)));
			int gridY = (int)(Math.floor((y - (height - padSize)) / (size / 3.0f)));
			char key = ' ';

			if (gridX == 0 && gridY == 2) { key = '1'; }
			if (gridX == 1 && gridY == 2) { key = '2'; }
			if (gridX == 0 && gridY == 1) { key = '3'; }
			if (gridX == 1 && gridY == 1) { key = '4'; }
			if (gridX == 0 && gridY == 0) { key = '5'; }
			if (gridX == 1 && gridY == 0) { key = 'e'; }

			Emulator.the.OnKey(key, down, false, false);
		}
	}
	public void Draw()
	{
		if(!active)
			return;
		if(Emulator.the.ReplayActive())
			return;
		final long passed_time = SystemClock.uptimeMillis() - touch_time;
		if(passed_time > 2000)
		{
			dir_x = 0;
			dir_y = 0;
			return;
		}
		final long passed_joy_time = SystemClock.uptimeMillis() - touch_joy_time;
		if(pid_joy < 0 && passed_joy_time > 30)
		{
			dir_x *= 0.5f;
			dir_y *= 0.5f;
		}

		final float alpha = passed_time > 1000 ? (float)(2000 - passed_time)/1000.0f : 1.0f;

		//pad
		int padTex = isometric == 0 ? textures[texJoy] : textures[texJoyIsometric];
		Emulator.the.GLSpriteDraw(sprite, padTex, 0, padShift, size, size, 0.3f*alpha, false);
		//pointer
		Emulator.the.GLSpriteDraw(sprite, textures[texButton], (int)(dir_x*0.25f*size), (int)(padShift + (dir_y*0.25f*size)), size, size, 0.3f*alpha, false);

		//fire
		Emulator.the.GLSpriteDraw(sprite, textures[texButton], width - size, (int)(halfHeight - halfSize), size, size, 0.5f*alpha, false);
		//jump
		if (isometric == 0)
		{
			Emulator.the.GLSpriteDraw(sprite, textures[texButton], width - size, (int)(upperQuarter - halfSize), size, size, 0.3f*alpha, false);
		}
		//space
		Emulator.the.GLSpriteDraw(sprite, textures[texButton], width - size, (int)(lowerQuarter - halfSize), size, size, 0.3f*alpha, false);

		Emulator.the.GLSpriteDraw(sprite, textures[texWeaponRack], 0, (int)(height - padSize), size, size, 0.3f*alpha, false);
	}
	public void Active(boolean on)
	{
		if(active && !on)
		{
			dir_x = 0;
			dir_y = 0;
			Emulator.the.OnKey('r', false, false, false);
			Emulator.the.OnKey('l', false, false, false);
			Emulator.the.OnKey('u', false, false, false);
			Emulator.the.OnKey('d', false, false, false);
			Emulator.the.OnKey('f', false, false, false);
			pid_joy = -1;
		}
		super.Active(on);
	}
}
